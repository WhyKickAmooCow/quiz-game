Pulling and running from Docker:
	
	docker run -it whykickamoocow/quiz-game

Building and running from Docker:
	
	docker build -t imagename .
	docker run -it imagename


Running natively (Tested with python 3.5.3 Fedora 25 (Should work on MacOS, Windows BSD etc. but not guranteed): 

	python main.py

##### Docker installation instructions for [Windows](https://docs.docker.com/docker-for-windows/install/), [Mac](https://docs.docker.com/docker-for-mac/install/) and [Linux (Multiple distros)](https://docs.docker.com/engine/installation/#server).
##### Installation overview and supported platforms [here](https://docs.docker.com/engine/installation/).

##### Docker getting started guide is within the installation guide page for your respective OS