#!/usr/bin/env python3

# Written by Adam Spurgeon.
# AS 91373
# Term 3 2017

# This program is a quiz game that:
# - asks the user if they want to play the game.
# - asks for the users name.
# - asks for how many questions they want to play.
# - asks them randomly selected questions.
# - if they answer a question right add one to their score and say they got it correct.
#   if not tell them they got it incorrect and do not modify their score.
# - when the game is over, print their score, show leaderboard/highscores, and ask if they want to play again.

import random, os, time, signal
from operator import itemgetter

# Define valid string values to be used in place of booleans (used in the to_bool function)
boolAnswers = {'true': ['y', 'yes', 'true'], 'false': ['n', 'no', 'false']}

gameMinLen = 1
gameMaxLen = 10

# Seonds to wait until clearing the screen when the user has answered a question in the game
screenRefreshTime = 0.5
# Message to show to the user when exiting the application
exitMsg = 'Goodbye!'

class Question:
	def __init__(self, question, answer, options = []):
		self.question = question
		self.answer = answer
		self.boolAnswer = type(answer) == bool

		# If answer is a boolean and options array is provided, throw an error as the user may not have intended to do this.
		if self.boolAnswer:
			if options:
				raise ValueError('options array passed but answer is of type boolean')
			self.options = []
		else:
			# Ensure that answer is valid given the options array provided when answer is not a boolean.
			if options:
				if type(answer) != int:
					raise TypeError('if question is multi-choice then answer must be an integer corresponding to the index of the correct answer in options array')
				elif answer not in range(len(options)):
					# Answer must be an int within length of options array if answer is not a boolean and options array is provided
					raise ValueError('answer must be a valid index in options array')
			self.options = options


	def answer_is_correct(self, answer):
		return self.answer == answer


# Clear screen with optional delay argument for seconds to sleep until clearing screen
def clear(*delay):
	if delay:
		time.sleep(delay[0])
	# Clear screen for both Unixs (Linux, MacOS, BSD) and Windows
	os.system('cls' if os.name == 'nt' else 'clear')

# Takes string value returns boolean equivalent
def to_bool(value):
		# Handle values that are already a boolean, return boolean and do no other checks
		if type(value) == bool:
			return value
		# Ensure passed argument is a string (Only other type allowd is a bool.
		if not type(value) == str:
			raise TypeError('provided value is not a string')
		# If string contains a valid value stored in boolAnswers dict
		if value in boolAnswers['true']:
			return True
		elif value in boolAnswers['false']:
			return False
		else:
			# Cannot find value provided in boolAnswers dict
			raise ValueError('string provided cannot be converted to a boolean (check if value is equal to any value in boolAnswers)')

# Accept user input with defined criteria
def accept_input(lowercase = True, rmWhitespace = True, allowedValues = [], asType = None, promptPrefix = '--> ', allowEmpty = False, defaultVal = ''):
	# Basic parameter checks so it is safe to make some assumptions within the loop
	if asType == str:
		raise ValueError('Setting asType to string is redundant and unsupported')
	if asType and asType != int and allowedValues and type(allowedValues) != range:
		raise ValueError('asType and allowedValues can only coexist if allowedValues is a range and asType is set to int')
	if allowEmpty:
		if not defaultVal:
			raise ValueError('default value must be provided if emtpy input is allowed')
		if allowedValues and defaultVal not in allowedValues:
			raise ValueError('default value must in in allowedValues')

	# 'While True' loop needed because python has no 'do ... while' loop
	while True:
		# Get user input and display specified prefix for response area
		answer = input(promptPrefix)

		if rmWhitespace:
			answer = ''.join(answer.split())

		if lowercase:
			answer = answer.lower()
		# If answer string lengths is longer than 0
		if answer:
			# If no further processing needed
			if not asType and not allowedValues:
				break
		# If user input is empty, check if this is allowed
		# and if it is replace user input with defaultVal string
		elif allowEmpty:
			answer = defaultVal
			# No further processing needed
			if type(answer) != str:
				break
		else:
			# If user input is empty, print error and do not break while loop
			print("Please enter some text")
			continue

		# Only accept asType specified value type
		if asType:
			if asType == int:
				try:
					answer = int(answer)
				except ValueError:
					print('Please enter an integer')
					continue
			elif asType == bool:
				try:
					# Break loop on valid boolean answer as there will be no further input proccessing needed
					answer = to_bool(answer)
					break
				except ValueError:
					print('Please enter a valid boolean value')
					continue
		# If allowedValues length is greater than 0
		if allowedValues:
			# If user input is not found in allowedValues then print error and ask for input again
			if answer in allowedValues:
				break
			else:
				print('Allowed values are: ', list(allowedValues))
				continue

	# Once while loop has been broken, return user's answer (May be modified from actions in the loop)
	return answer

# Asks provided question
def ask_question(question):
	print(question.question)

	# Print option array as a human readable list
	if not question.boolAnswer:
		for i in range(len(question.options)):
			print(i,')', question.options[i])

	# allowedValues is redundant when asType is set to boolean so if answer is a boolean, ommit this value.
	answer = accept_input(
		allowedValues = None if question.boolAnswer else range(len(question.options)),
		asType = bool if question.boolAnswer else int
		)

	return question.answer_is_correct(answer)

# Handle program being killed using SIGINT. (CTRL+C most likely in this case)
def signal_handler(signal, frame):
        print('\n\n', exitMsg, '\n')
        exit(0)
signal.signal(signal.SIGINT, signal_handler)


def main():
	clear()

	print(
	'''\
Instructions:
	In this game you will be asked a series of questions that can be yes, no, or multi-choice.

	To enter an answer for a yes/no question, simply type in y, yes, true or n, no, false.
	For a multi-choice answer, enter the number of the option e.g.

	Vim or Emacs
	0 ) Vim
	1 ) Emcas
	2 ) Nano
	--> 2

	To exit this game press CTRL + C on the keyboard.

	To continue to the game press ENTER/RETURN on the keyboard.

	Do you want to play?\
	''')

	if not accept_input(asType = bool, allowEmpty = True, defaultVal = 'y'):
		exit()

	highscores = []

	# Game run function to be called recursively
	def game():
		# To keep track of score
		score = 0
		# Array of questions to be asked
		questionArr = [
			Question('A string is an array of what?', 0, options = ['characters', 'integers', 'floats']),
			Question('What conditional statement is a ternary operator the equivalent of?', 0, options = ['if(x) then y else z', 'for(i in x) do y', 'while(x) do y']),
			Question('The "continue" statement when placed in a loop does: ', 1, options = ['ends the loop', 'ends the current iteration of the loop', 'skips 3 lines ahead for execution']),
			Question('In Javascript all numberic values are stored as one datatype, what is it?', 2, options = ['int', 'long', 'float']),
			Question('A binary search has running time of: ', 2, options = ['O(n)', 'O(n^2)', 'O(log2 n)', 'O(n^n)']),
			Question('x === y only returns true when x and y store the same value and are of the same datatype', True),
			Question('HTTP status code 418 is "I am a teapot" and is defined in the "Hyper Text Coffee Pot Control Protocol"', True),
			Question('Javascript is a statically typed language', False),
			Question('A variable with a value of NULL is equal to a variable with a value of 0', False),
			Question('Javascript supports method chaining over multiple lines if lines do not end with a semicolon', True),
			Question('Javascript is a compiled language', False),
			Question('The statement "x && y" only returns true if both x and y are true', True),
			Question('The staement "x || y" returns true if either x or y is true', True),
			Question('Promises are constructs designed to replace callbacks for handling asynchonous calls in Javascript', True),
			Question('Javascript is usually either run using a JIT compiler or an interpreter', True),
			Question('A Javascipt function can take the return value of another function as a parameter', True),
			Question('A Javascript function can call itself from inside the function', True),
			Question('Infinity is a valid value in Javascript', True),
			Question('Javascript is an "Object Oriented" programming language', True),
			Question('[] + [] in Javascript equals ""', True)
			]

		# Initial clear
		clear()

		# Allow highscores variable to be accessed and modified within nested scopes without using variable capture in closures
		nonlocal highscores

		print('Welcome to a quiz game! \n')
		print('Your name: ')

		# Will allow special characters and numbers because some player might want to use a username instead of their real name
		name = accept_input(lowercase = False)

		print('\nGood luck ', name, '!')

		clear(screenRefreshTime)

		print('How many questions would you like to play? (%d-%d)' % (gameMinLen, gameMaxLen))

		# Only accept integers between game min length and max length (+1 needed due to the way int membership in range works (< max))
		gameLength = accept_input(allowedValues = range(gameMinLen, gameMaxLen + 1), asType = int)

		clear()

		# For the duration of the user provided game length
		for i in range(gameLength):
			if not len(questionArr):
				break
			# Generate a random number within valid indices of question array to choose random question
			question_index = random.randint(0, len(questionArr)-1)

			print("Question %d/%d, (Current score: %d): " % (i+1, gameLength, score))

			if ask_question(questionArr[question_index]):
				print('Correct!')
				score += 1
			else:
				print("Incorrect!")

			# Delete question asked from question array to ensure it won't be asked again in the same game
			del questionArr[question_index]

			# Clear X seconds after each question is answered
			clear(screenRefreshTime)

		if highscores:
			print('Highscores: ')

			for i in range(len(highscores)):
				print(highscores[i]['score'], ': ', highscores[i]['name'])

		# Add a dictionary to the highscores array with player name and score
		highscores.append({'name': name, 'score': score})

		# Display player's score as well as a congratulations for beating the high score
		# if player score is greater than the score stored at highscores index 0, is by definition
		# the high score because 'highscore' array is sorted in order of score below
		print('\nYour score: ', score, ' New highscore!' if (score > highscores[0]['score'] or not highscores) else '')

		# Sort highscores array by score (Highest scores at array start)
		highscores = sorted(highscores, key = itemgetter('score'), reverse = True)

		# Recursive call to game() if player wants to play again
		print('\nPlay again? ')
		if accept_input(asType = bool):
			game()

	game()
	# Only print once inital game() has finished as the game() function can call itself recursively
	print('\n', exitMsg, '\n')


main()
