FROM python:3.5.3-alpine

RUN addgroup -S nupp && adduser -S -g nupp nupp

RUN apk update && \
	apk add build-base ncurses-dev bash &&\
	python -m ensurepip &&\
	pip install readline

COPY ./inputrc /etc/inputrc

ENV HOME=/home/nupp

COPY main.py $HOME/

RUN chown -R nupp:nupp $HOME/*

WORKDIR $HOME

USER nupp

CMD ["python", "main.py"]